package TestCaseforLogin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import Pages.LoginPage;

public class VerifyLogin {
	private boolean isAuthenticated = false;
	private WebDriver driver = null;

	public VerifyLogin(WebDriver driver)
	{
		this.driver = driver;
	}
	
	public VerifyLogin()
	{
		driver = new ChromeDriver();
		driver.get("https://alexa89.atlassian.net/");
	}
	
	public boolean verifyValidLogin() throws InterruptedException {
		LoginPage login = new LoginPage(driver);
		isAuthenticated = login.loginToAtlassian("alexandramonica.moldovan@gmail.com", "alexa123");
		return isAuthenticated;
	}	
	
}