package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * @author alexandra.moldovan
 *
 *This class will store locators and methods of login
 */

public class CreateBlankDocument
{
	static By titleName=By.id("content-title");
	static By pageContent=By.id("tinymce");
		
	
	//create new page
	public static String createNewPage(WebDriver driver, String templateName) throws InterruptedException 
	{
		Thread.sleep(10000);
		String newPage = null;
	    WebElement addPageTitle = driver.findElement(By.xpath("//*[@id=\"title-0ccyagu\"]"));
	    WebElement addPage = (WebElement) ((JavascriptExecutor) driver).executeScript(
                "return arguments[0].parentNode;", addPageTitle);
	    addPage.click();
	    Thread.sleep(5000);
	    WebElement createPageButton = driver.findElement(By.cssSelector("html body#com-atlassian-confluence.theme-default.dashboard.aui-layout.aui-theme-default div#create-dialog.aui-popup.aui-dialog div.dialog-components div.dialog-button-panel button.create-dialog-create-button.aui-button.aui-button-primary"));
	    createPageButton.click();
	   newPage = driver.getCurrentUrl();
	  return newPage;
	}	
 
	
	//Create new page with new title and page content
	public static void updatePageContent(WebDriver driver, String title, String PageText) throws InterruptedException 
	{
	    driver.findElement(titleName).clear();
	    driver.findElement(titleName).sendKeys(title);
	    WebElement contentElement = driver.findElement(By.xpath("//*[@id=\"wysiwygTextarea_ifr\"]"));
	    driver.switchTo().frame(contentElement);
	    driver.findElement(pageContent).clear();
	    driver.findElement(pageContent).sendKeys(PageText);
	    driver.switchTo().defaultContent();
	    driver.findElement(By.id("rte-button-publish")).click();
	}
	
}

		
