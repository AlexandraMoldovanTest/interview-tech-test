package Pages;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;



public class ModifyPageSecurity
{
	
		public static void modifySecurity(WebDriver driver, String spaceName, String pageName, UserPermission userSecurity, boolean isAdd) throws InterruptedException
		{
			
			//Find your Space
			Thread.sleep(7000);
			WebElement homePageTitle = driver.findElement(By.xpath("//*[@id=\"title-ncwtri2\"]"));   
			WebElement homePage = (WebElement) ((JavascriptExecutor) driver).executeScript(
	                "return arguments[0].parentNode;", homePageTitle);
			homePage.click();
			Thread.sleep(5000);
			WebElement requestedSpace = driver.findElement(By.tagName("div")).findElement(By.linkText(spaceName));
			requestedSpace.click();
			
			
			//Select "Pages" in the Space
			Thread.sleep(5000);
			driver.findElement(By.xpath("//*[@id=\"confluence-left-nav\"]/div[1]/div[1]/div/div[2]/div/div[3]/div/div/div/a[2]/div/button/div/div[2]/div[1]")).click();
			
			
			//find Page by name 
			Thread.sleep(5000);
			driver.findElement(By.linkText(pageName)).click();
			
			//Find "restriction" button on the page
			Thread.sleep(5000);
			driver.findElement(By.xpath("//*[@id=\'content-metadata-page-restrictions\']")).click();
		
			
			//select 2nd option on drop down (Editing Restricted) 
			Thread.sleep(5000);
			WebElement dropDown = driver.findElement(By.className("restrictions-dialog-option"));
			dropDown.click();
			WebElement editingRestricted = driver.findElement(By.xpath("//*[@id=\"select2-drop\"]/ul/li[2]/div/div/div"));
			editingRestricted.click();
			if (isAdd)
			{
				addPermission(driver, userSecurity);
			}
			else
			{
				removePermission(driver, userSecurity.Username());
			}
		}
		
		//add user permission
		public static void addPermission(WebDriver driver, UserPermission userSecurity) throws InterruptedException
		{
			By enterUser=By.xpath("//*[@id=\"s2id_autogen2\"]");
			WebElement enterUserElement = driver.findElement(enterUser);
			enterUserElement.clear();
			enterUserElement.sendKeys(userSecurity.Username());
			Thread.sleep(1000);
			enterUserElement.sendKeys(Keys.RETURN);
			Thread.sleep(1000);
		    driver.findElement(By.id("page-restrictions-add-button")).click();
		    Thread.sleep(1000);
		    driver.findElement(By.id("page-restrictions-dialog-save-button")).click();
		}	
		
		//remove user permission
		public static void removePermission(WebDriver driver, String userName) throws InterruptedException
		{
			WebElement existingUserPermission = driver.findElement(By.linkText(userName));
			WebElement existingUserPermissionColumn = (WebElement) ((JavascriptExecutor) driver).executeScript(
	                "return arguments[0].parentNode;", existingUserPermission);
			WebElement existingUserPermissionRow = (WebElement) ((JavascriptExecutor) driver).executeScript(
	                "return arguments[0].parentNode;", existingUserPermissionColumn);
			existingUserPermissionRow.findElement(By.xpath("//button[contains(.,'Remove')]")).click();
		    driver.findElement(By.id("page-restrictions-dialog-save-button")).click();
		    
		    
		}		
} 



