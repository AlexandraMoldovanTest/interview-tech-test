import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import TestCaseforLogin.VerifyLogin;
import TestCaseforPageCreation.VerifyCreatePage;
import TestCaseforSecurity.VerifyPageSecurity;



public class LaunchApplication 
{

	public static void main(String[] args) throws InterruptedException 
	
	{
		
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"/chromedriver");
		
		WebDriver driver=new ChromeDriver();
		boolean isAuthenticated;
		driver.get("https://alexa89.atlassian.net/");
		VerifyLogin newLoginTest = new VerifyLogin(driver);
		isAuthenticated = newLoginTest.verifyValidLogin();      
		if (isAuthenticated)
		{
			VerifyCreatePage newCreatePage = new VerifyCreatePage(driver, isAuthenticated);
			newCreatePage.verifyCreateNewPage();
			VerifyPageSecurity pageSecurity  = new VerifyPageSecurity(driver, isAuthenticated);
			pageSecurity.verifyModifyPageSecurity();
		}

	}

}
 

