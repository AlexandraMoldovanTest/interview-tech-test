package TestCaseforPageCreation;

import java.util.Date;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import Pages.CreateBlankDocument;

public class VerifyCreatePage {
	private boolean isAuthenticated = false;
	private WebDriver driver = null;

	public VerifyCreatePage(WebDriver driver, boolean isAutenticated)
	{
		this.driver = driver;
		this.isAuthenticated = isAutenticated;
	}
	
	public VerifyCreatePage()
	{
		driver = new ChromeDriver();
		driver.get("https://alexa89.atlassian.net/");
	}
	
	//this adds a new Page with a unique name and body content
	public void verifyCreateNewPage() throws InterruptedException {
		if (isAuthenticated) {
			CreateBlankDocument.createNewPage(driver, "");
			Date currentDateTime = new java.util.Date();
			CreateBlankDocument.updatePageContent(driver, "New Page - " + currentDateTime, "This is a test");
		}
	}
}
