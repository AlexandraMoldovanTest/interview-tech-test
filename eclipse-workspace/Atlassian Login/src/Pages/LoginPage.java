package Pages;
/**
 * 
 */


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * @author alexandra.moldovan
 *
 *This class will store locators and methods of login
 */

public class LoginPage
{
	WebDriver driver;
	
	By username=By.id("username");
	By password=By.id("password");
	By loginButton=By.name("login-submit");
	
	
	public LoginPage(WebDriver driver)
	{
		//initialize driver page above	
		this.driver=driver;	
	}
	
	//login using username and password
	public boolean loginToAtlassian (String userid, String passid) throws InterruptedException
	{
		driver.findElement(username).sendKeys(userid);
		driver.findElement(By.id("login-submit")).click();
		Thread.sleep(500);
		driver.findElement(password).sendKeys(passid);
		driver.findElement(By.id("login-submit")).click();
		return true;
		
	}
			
}
