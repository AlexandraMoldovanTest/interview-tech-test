package TestCaseforSecurity;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import Pages.ModifyPageSecurity;
import Pages.UserPermission;
import Pages.UserPermission.PermissionType;

public class VerifyPageSecurity {
	private boolean isAuthenticated = false;
	private WebDriver driver;
	
	public VerifyPageSecurity(WebDriver driver, boolean isAuthenticated)
	{
		this.driver = driver;
		this.isAuthenticated = isAuthenticated;
	}
	
	public VerifyPageSecurity()
	{
		driver = new ChromeDriver();
		driver.get("https://alexa89.atlassian.net/");
	}
	
	//find Page "Alex"
	public void verifyModifyPageSecurity() throws InterruptedException {
		if (isAuthenticated) {
			UserPermission userSecurity = new UserPermission("ale.alexa89", PermissionType.CanEdit);
			ModifyPageSecurity.modifySecurity(driver, "Atlassian Interview", "Alex", userSecurity, true);
			ModifyPageSecurity.modifySecurity(driver, "Atlassian Interview", "Alex", userSecurity, false);
		}
	}
}
