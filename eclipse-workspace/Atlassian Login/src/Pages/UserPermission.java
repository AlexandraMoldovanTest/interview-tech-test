package Pages;

public class UserPermission
{
	
	private String userName;
	private PermissionType permission;
	
	public UserPermission(String userName, PermissionType permission)
	{
		this.userName = userName;
		this.permission = permission;
	}
	
	public String Username()
	{
		return userName;
	}
	
	public PermissionType Permission()
	{
		return permission;
	}
	
	public enum PermissionType
	{
		CanView,
		CanEdit;
	}
}